%global glib2_version %(pkg-config --modversion glib-2.0 2>/dev/null || echo bad)
%global qmi_version %(pkg-config --modversion qmi-glib 2>/dev/null || echo bad)
%global mbim_version %(pkg-config --modversion mbim-glib 2>/dev/null || echo bad)


Name:           ModemManager
Version:        1.22.0
Release:        2
Summary:        Mobile broadband modem manager
License:        GPLv2+
URL:            https://www.freedesktop.org/wiki/Software/ModemManager/
Source:         https://gitlab.com/linux-mobile-broadband/ModemManager/-/archive/%{version}/%{name}-%{version}.tar.bz2

BuildRequires:  meson >= 0.53 dbus-devel gtk-doc vala-tools vala-devel
BuildRequires:  gettext-devel glib2-devel gobject-introspection-devel libgudev-devel
BuildRequires:  libqmi-devel >= 1.34.0 libmbim-devel >= 1.30.0 systemd-devel systemd
BuildRequires:  python3-gobject python3-dbus pkgconfig(polkit-gobject-1)

Requires:       libmbim-utils libqmi-utils glib2 >= %{glib2_version}
Requires:       libmbim >= %{mbim_version} libqmi >= %{qmi_version}
Requires:       %{name}-glib = %{version}-%{release}
%{?systemd_requires}

%description
ModemManager is a DBus-activated daemon which controls mobile broadband
(2G/3G/4G) devices and connections. Whether built-in devices, USB dongles,
bluetooth-paired telephones, or professional RS232/USB devices with
external power supplies, ModemManager is able to prepare and configure
the modems and setup connections with them.

%package        devel
Summary:        Development package for ModemManager
Requires:       %{name} = %{version}-%{release} pkgconfig

%description    devel
%{name}-devel contains header files and libraries for %{name}.

%package        glib
Summary:        Libraries for applications using glib to use ModemManager
Requires:       glib2 >= %{glib2_version}

%description    glib
Libraries for applications using glib to use ModemManager.

%package        glib-devel
Summary:        Development package for applications using glib to use ModemManager
Requires:       pkgconfig
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}
Requires:       %{name}-glib = %{version}-%{release}
Requires:       glib2-devel >= %{glib2_version}

%description    glib-devel
Development package for applications using glib to use ModemManager.

%package        vala
Summary:        Vala bindings for ModemManager
Requires:       vala
Requires:       %{name}-glib = %{version}-%{release}

%description vala
Vala bindings for ModemManager

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson \
-Ddist_version='"%{version}-%{release}"' \
-Dvapi=true \
-Dqrtr=false \
-Dgtk_doc=true \
-Dpolkit=permissive \
-Dbash_completion=false
%meson_build

%install
%meson_install
find %{buildroot}%{_datadir}/gtk-doc |xargs touch --reference meson.build
%find_lang %{name}
mkdir -p %{buildroot}%{_datadir}/bash-completion/completions/
cp -a cli/mmcli-completion %{buildroot}%{_datadir}/bash-completion/completions/mmcli
%delete_la

%check
%meson_test

%post
%systemd_post ModemManager.service

%preun
%systemd_preun ModemManager.service

%ldconfig_scriptlets glib

%files
%defattr(-,root,root)
%doc AUTHORS
%license COPYING COPYING.LIB  
%{_bindir}/mmcli
%{_sbindir}/ModemManager
%dir %{_libdir}/%{name}
%{_libdir}/%{name}/*.so
%{_datadir}/locale/*
%{_udevrulesdir}/*
%{_unitdir}/ModemManager.service
%{_datadir}/dbus-1/system-services/org.freedesktop.ModemManager1.service
%{_datadir}/bash-completion
%{_datadir}/polkit-1/actions/org.freedesktop.ModemManager1.policy
%{_sysconfdir}/dbus-1/system.d/org.freedesktop.ModemManager1.conf
%{_datadir}/ModemManager

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/ModemManager/

%files glib
%defattr(-,root,root)
%{_libdir}/libmm-glib.so.*
%{_libdir}/girepository-1.0/*.typelib

%files glib-devel
%defattr(-,root,root)
%{_libdir}/libmm-glib.so
%{_libdir}/pkgconfig/mm-glib.pc
%{_includedir}/libmm-glib/*.h
%{_datadir}/gtk-doc/html/libmm-glib/*
%{_datadir}/gir-1.0/*.gir

%files vala
%defattr(-,root,root)
%{_datadir}/vala/vapi/libmm-glib.*

%files help
%defattr(-,root,root)
%doc README NEWS
%{_mandir}/man8/*
%{_mandir}/man1/*
%{_datadir}/icons/hicolor/22x22/apps/*.png
%{_datadir}/gtk-doc/html/*/*
%{_datadir}/dbus-1/interfaces/*.xml

%changelog
* Wed Mar 13 2024 panchenbo <panchenbo@kylinsec.com.cn> - 1.22.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix build error: add missing BuildRequires:pkgconfig(polkit-gobject-1)

* Tue Jan 02 2024 gaihuiying <eaglegai@163.com> - 1.22.0-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update ModemManager to 1.22.0

* Mon Jul 24 2023 gaihuiying <eaglegai@163.com> - 1.20.6-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update ModemManager to 1.20.6

* Fri Nov 18 2022 gaihuiying <eaglegai@163.com> - 1.18.12-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update ModemManager to 1.18.12

* Sat Nov 12 2022 gaihuiying <eaglegai@163.com> - 1.14.8-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: fix test failure because of glib2 switching to pcre2

* Wed Jan 27 2021 xihaochen <xihaochen@huawei.com> - 1.14.8-1
- Type:requirements
- ID:NA
- SUG:NA
- DESC: update ModemManager to 1.14.8

* Thu Dec 03 2020 liulong <liulong20@huawei.com> - 1.14.0-9
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix service startup failure.

* Fri Jul 17 2020 cuibaobao <cuibaobao1@huawei.com> - 1.14.0-8
- Type:update
- Id:NA
- SUG:NA
- DESC:update to 1.14.0

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.0-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file

* Thu Sep 05 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.0-6
- Type:enhance
- ID:NA
- SUG:NA
- DESC:new rule

* Thu Aug 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.8.0-5
- Package init
